# imports
import random #sirve para generar numeros aleatorios
import os

reservas = []


def validar(lista, num):  # validar si la habitacion ya se encuentra ocupada
    flag = False
    for reg in lista:
        if num in reg and reg[7] == 'pendiente':
            flag = True
            break
    return flag


def regReservas():  # Permite registrar nuestras reservas
    registro = []
    numH = 0
    band = True

    registro.append(len(reservas) + 1)  # codigo de reserva

    name = input("Ingrese el nombre del cliente: ")
    registro.append(name)

    numDias = int(input("Ingrese el numero de dias: "))
    registro.append(numDias)

    precioH = float(input("Ingrese el precio por dia: "))
    registro.append(precioH)

    sTotal = numDias * precioH
    registro.append(sTotal)

    tipoH = input("Tipo de habitación: (simple, doble): ")
    registro.append(tipoH)

    while band:
        if tipoH == 'simple':
            numH = random.randrange(101, 120)
        elif tipoH == 'doble':
            numH = random.randrange(201, 220)
        band = validar(reservas, numH)
    registro.append(numH)

    estado = input("Estado de reserva (pendiente, pagada, anulada): ")
    registro.append(estado)
    reservas.append(registro)
    with open("data/reserva.txt", "w") as temp_reserva:
        for reserva in reservas:
            temp_reserva.write("---------Datos de Persona-----------" + os.linesep)
            for reg in reserva:
                temp_reserva.write(str(reg) + os.linesep)
    menuPrincipal()


def menuPrincipal():  # Menu principal

    print("1. Registro de Reservas.")
    print("2. Consulta Reservas.")
    print("3. Eliminación de Reservas.")
    print("4. Pago de reservas.")
    print("5. Reporte de reservas.")
    print("0. Salir")

    resp = int(input("Elija una opción: "))

    if resp == 1:
        regReservas()
    elif resp == 2:
        print("2")
    elif resp == 3:
        print("3")
    elif resp == 4:
        print("4")
    elif resp == 5:
        print(reservas)
        menuPrincipal()
    elif resp == 0:
        print("Good Bye")
    else:
        print("opcion Incorrecta\n")
        menuPrincipal()


def main():  ##funcion principal

    menuPrincipal()

main()

